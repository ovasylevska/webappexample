FROM gradle:jdk8-alpine as GRADLE_BUILDER
WORKDIR /home/gradle/src
COPY . /home/gradle/src
USER root
RUN chown -R gradle .
USER gradle
RUN gradle build --info

FROM openjdk:8
VOLUME /tmp
EXPOSE 8084
COPY /src/main/resources/application-develop.properties ./application-develop.properties
COPY /src/main/resources/application-master.properties ./application-master.properties
COPY --from=GRADLE_BUILDER /home/gradle/src/build/libs/WebAppExample-1.0-SNAPSHOT.jar /
#ENTRYPOINT ["java","-Xmx1024m","-jar","WebAppExample-1.0-SNAPSHOT.jar"]
ENTRYPOINT exec java $JAVA_OPTS -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE -jar WebAppExample-1.0-SNAPSHOT.jar
