package com.webapp.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;

@RestController
@RequestMapping(value = "/ip", produces = "application/json")
public class IpController {

    @GetMapping(value = "private")
    public String current() {
        return getIpAddress();
    }

    private static String getIpAddress() {
        try {
            final InetAddress i = InetAddress.getLocalHost();
            return i.getHostAddress();
        } catch (Exception e) {
            return "undefined";
        }
    }
}
