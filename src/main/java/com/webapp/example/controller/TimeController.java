package com.webapp.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping(value = "/time", produces = "application/json")
public class TimeController {

    @GetMapping(value = "current")
    public String current() {
        return LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
    }

    @GetMapping(value = "current/utc")
    public String current_utc() {
        return getCurrentDateTimeInUtc().format(DateTimeFormatter.ISO_DATE_TIME);
    }

    private static LocalDateTime getCurrentDateTimeInUtc() {
        final ZonedDateTime now = ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault());
        final ZonedDateTime nowUtc = now.withZoneSameInstant(ZoneOffset.UTC);
        return nowUtc.toLocalDateTime();
    }
}
