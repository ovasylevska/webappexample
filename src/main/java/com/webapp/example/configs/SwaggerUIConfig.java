package com.webapp.example.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerUIConfig {
    private static final Logger logger = LoggerFactory.getLogger(SwaggerUIConfig.class);
    private @Value("${server.port}")
    int serverPort;
    private @Value("${server.context}")
    String serverContext;
    @Value("${spring.application.server}")
    private String applicationServer;

    @Bean
    public Docket api(ApiInfo apiInfo) {
        logger.info("Checkout 'http://{}:{}/swagger-ui.html#/' for swagger-ui", serverContext, serverPort);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        final ApiInfoBuilder builder = new ApiInfoBuilder();
        builder
                .title("Web App Example - Swagger UI (" + applicationServer + ")")
                .version("1.0");
        return builder.build();
    }
}
